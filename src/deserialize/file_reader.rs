use std::collections::HashMap;
use std::path::PathBuf;

pub trait FileReader {
    fn read_file<P: AsRef<str>>(&mut self, path: P) -> Result<String, std::io::Error>;
}

impl<'f, F> FileReader for &'f mut F
where
    F: FileReader,
{
    fn read_file<P: AsRef<str>>(&mut self, path: P) -> Result<String, std::io::Error> {
        (*self).read_file(path)
    }
}

#[derive(Debug, Clone, Default, Copy)]
pub struct FSReader;

impl FileReader for FSReader {
    fn read_file<P: AsRef<str>>(&mut self, path: P) -> Result<String, std::io::Error> {
        use std::fs::File;
        use std::io::Read;
        let mut res = String::new();
        let path = PathBuf::from(path.as_ref());

        match File::open(&path)?.read_to_string(&mut res) {
            Ok(_) => Ok(res),
            Err(e) => {
                eprintln!("Couldn't read file: {:?}", path);
                Err(e)
            }
        }
    }
}

pub struct CacheReader<F: FileReader> {
    pub cache: HashMap<String, String>,
    pub reader: F,
}

impl<F: FileReader + Default> Default for CacheReader<F> {
    fn default() -> Self {
        Self::new(F::default())
    }
}

impl<F: FileReader> FileReader for CacheReader<F> {
    fn read_file<P: AsRef<str>>(&mut self, path: P) -> Result<String, std::io::Error> {
        let path = path.as_ref();
        if !self.cache.contains_key(path) {
            let data = self.reader.read_file(path)?;
            self.cache.insert(path.to_string(), data);
        }
        Ok(self.cache[path].clone())
    }
}

impl<F: FileReader> CacheReader<F> {
    pub fn new(reader: F) -> Self {
        Self {
            cache: HashMap::new(),
            reader,
        }
    }
}
