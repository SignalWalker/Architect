use crate::Rule;

#[derive(Debug)]
pub enum BuildError {
    NoRef,
    NoImport,
    IO(std::io::Error),
    HeteroMod,
    Unmoddable,
    Indexer,
    Parse(pest::error::Error<Rule>),
}

impl From<pest::error::Error<Rule>> for BuildError {
    fn from(e: pest::error::Error<Rule>) -> Self {
        Self::Parse(e)
    }
}

impl From<std::io::Error> for BuildError {
    fn from(e: std::io::Error) -> Self {
        Self::IO(e)
    }
}
