use crate::deserialize::BuildError;
use crate::number::Number;
use crate::Rule;
use crate::StnParser;
use pest::iterators::Pair;
use pest::Parser;
use std::collections::HashMap;
use std::fmt::Display;
use std::io::Read;
use std::path::PathBuf;

#[derive(Debug, Clone)]
pub enum AST<'s> {
    // Literals
    Str(&'s str),
    Num(Number),
    Bool(bool),
    // Collections
    List(Vec<Self>),
    Class(&'s str, HashMap<&'s str, Option<Self>>),
    Mod(Box<Self>, Box<Self>),
    Tuple(Option<&'s str>, Vec<Self>),
    Dict(HashMap<&'s str, Option<Self>>),
    // Expressions
    Take(String),
    Ident(&'s str),
    Index(Box<Self>, Vec<Self>),
    // Meta
    NamedObj(&'s str, Box<Self>),
    Root(Vec<Self>),
}

impl<'s> AST<'s> {
    pub fn parse_file<P: AsRef<str>>(
        path: P,
        data: &'s mut String,
    ) -> Result<(String, Self), BuildError> {
        let path = path.as_ref();
        std::fs::File::open(path)?.read_to_string(data)?;
        let res = Self::parse(path, data)?;
        Ok((path.to_string(), res))
    }

    // TODO :: Interruptable parsing
    pub fn parse<P: AsRef<str>>(path: P, data: &'s str) -> Result<Self, BuildError> {
        let mut res = StnParser::parse(Rule::Stn, &data)?
            .filter_map(|p| {
                // dbg!(&p);
                match AST::parse_pair(path.as_ref(), p) {
                    Ok(a) => {
                        // dbg!(&a);
                        // eprintln!("AST::Parse -> AST");
                        Some(a)
                    }
                    Err(ref p) if p.as_rule() == Rule::EOI => {
                        // eprintln!("AST::Parse -> EOI");
                        None
                    }
                    Err(p) => {
                        eprintln!("Error on Rule: {:?}", p.as_rule());
                        None
                    }
                }
            })
            .collect::<Vec<_>>();
        Ok(res.remove(0))
    }

    fn parse_pair(path: &str, pair: Pair<'s, Rule>) -> Result<Self, Pair<'s, Rule>> {
        fn pair_to_dict<'s>(
            path: &str,
            dict: Pair<'s, Rule>,
        ) -> Result<HashMap<&'s str, Option<AST<'s>>>, Pair<'s, Rule>> {
            let mut res = HashMap::new();
            for (key, val) in dict.into_inner().map(|e| {
                let mut inner = e.into_inner();
                (
                    inner.next().unwrap().as_str(),
                    inner.next().map(|pair| AST::parse_pair(path, pair)),
                )
            }) {
                match val {
                    None | Some(Ok(_)) => {
                        res.insert(key, val.map(|v| v.unwrap()));
                    }
                    Some(Err(p)) if p.as_rule() == Rule::EOI => {
                        eprintln!("Encountered EOI in pair_to_dict");
                    }
                    Some(Err(p)) => {
                        return Err(p);
                    }
                }
            }
            Ok(res)
        }
        fn pair_to_vec<'s>(
            path: &str,
            list: Pair<'s, Rule>,
        ) -> Result<Vec<AST<'s>>, Pair<'s, Rule>> {
            let mut res = Vec::new();
            for ast in list.into_inner().map(|pair| AST::parse_pair(path, pair)) {
                match ast {
                    Ok(v) => {
                        // eprintln!("pair_to_vec -> {:?}", v);
                        res.push(v)
                    }
                    Err(e) if e.as_rule() == Rule::EOI => {
                        // eprintln!("Encountered EOI in pair_to_vec");
                    }
                    Err(e) => return Err(e),
                }
            }
            Ok(res)
        }
        use Rule::*;
        match pair.as_rule() {
            // Literals
            String => Ok(AST::Str(pair.into_inner().next().unwrap().as_str())),
            Boolean => Ok(AST::Bool(pair.as_str().parse().unwrap())),
            Number => Ok(AST::Num(pair.as_str().parse().unwrap())),
            // Collections
            Class => {
                let mut inner = pair.into_inner();
                Ok(AST::Class(
                    inner.next().unwrap().as_str(),
                    pair_to_dict(path, inner.next().unwrap())?,
                ))
            }
            Dict => Ok(AST::Dict(pair_to_dict(path, pair)?)),
            List => Ok(AST::List(pair_to_vec(path, pair)?)),
            Modifier => {
                let mut inner = pair.into_inner();
                Ok(AST::Mod(
                    Box::new(Self::parse_pair(path, inner.next().unwrap())?),
                    Box::new(Self::parse_pair(path, inner.next().unwrap())?),
                ))
            }
            Tuple => {
                let mut inner = pair.into_inner().peekable();
                let mut vals = Vec::new();
                let ident = if inner.peek().unwrap().as_rule() == Rule::Ident {
                    Some(inner.next().unwrap().as_str())
                } else {
                    None
                };
                for val in inner.map(|pair| Self::parse_pair(path, pair)) {
                    match val {
                        Ok(v) => vals.push(v),
                        Err(e) => {
                            dbg!(e);
                        }
                    }
                }
                Ok(AST::Tuple(ident, vals))
            }
            // Expressions
            Ident => Ok(AST::Ident(pair.as_str())),
            Take => {
                let mut path = {
                    let mut path = PathBuf::from(path);
                    path.pop();
                    path
                };
                path.push(pair.into_inner().next().unwrap().as_str().to_owned());
                // println!("take {}", path);
                Ok(AST::Take(path.to_str().unwrap().to_owned()))
            }
            // Meta
            Index => {
                let mut inner = pair.into_inner();
                let first = AST::parse_pair(path, inner.next().unwrap())?;
                let mut indices = Vec::new();
                for n in inner {
                    indices.push(AST::parse_pair(path, n)?);
                }
                Ok(AST::Index(Box::new(first), indices))
            }
            Obj => {
                let mut inner = pair.into_inner();
                let first = AST::parse_pair(path, inner.next().unwrap())?;
                match first {
                    AST::Ident(id) => Ok(AST::NamedObj(
                        id,
                        Box::new(AST::parse_pair(path, inner.next().unwrap())?),
                    )),
                    _ => Ok(first),
                }
            }
            NamedObj => {
                let mut inner = pair.into_inner();
                Ok(AST::NamedObj(
                    inner.next().unwrap().as_str(),
                    Box::new(AST::parse_pair(path, inner.next().unwrap())?),
                ))
            }
            Stn => Ok(AST::Root(pair_to_vec(path, pair)?)),
            _ => Err(pair),
        }
    }
}

impl<'s> Display for AST<'s> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use AST::*;
        match self {
            Str(s) => write!(f, "\"{}\"", s)?,
            Ident(s) => write!(f, "{}", s)?,
            Bool(b) => write!(f, "{}", b)?,
            Num(n) => write!(f, "{}", n)?,
            Take(p) => write!(f, "take \"{}\"", p)?,
            Root(n) => {
                for node in n {
                    writeln!(f, "{};", node)?;
                }
            }
            List(n) => {
                write!(f, "[ ")?;
                for (i, node) in n.iter().enumerate() {
                    write!(f, "{}{}", node, if i != n.len() - 1 { ", " } else { "" })?;
                }
                write!(f, " ]")?;
            }
            Dict(m) => {
                write!(f, "{{ ")?;
                for (i, (k, v)) in m.iter().enumerate() {
                    write!(f, "{}", k)?;
                    if let Some(v) = v {
                        write!(f, ": {}", v)?;
                    }
                    if i != m.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, " }}")?;
            }
            Index(s, n) => {
                write!(f, "{}", s)?;
                for node in n.iter() {
                    write!(f, "[{}]", node)?;
                }
            }
            Mod(e, m) => write!(f, "{} < {}", e, m)?,
            Tuple(id, e) => {
                if let Some(id) = id {
                    write!(f, "{}", id)?;
                }
                write!(f, "(")?;
                for (i, el) in e.iter().enumerate() {
                    write!(f, "{}{}", el, if i != e.len() - 1 { ", " } else { "" })?;
                }
                write!(f, ")")?;
            }
            Class(id, m) => {
                write!(f, "{} {{", id)?;
                for (i, (k, v)) in m.iter().enumerate() {
                    write!(f, "{}", k)?;
                    if let Some(v) = v {
                        write!(f, ": {}", v)?;
                    }
                    if i != m.len() - 1 {
                        write!(f, ", ")?;
                    }
                }
                write!(f, ")")?;
            }
            NamedObj(id, obj) => {
                write!(f, "{}: {}", id, obj)?;
            }
        };
        Ok(())
    }
}
