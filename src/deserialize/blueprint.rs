use crate::deserialize::BuildError;
use crate::number::Number;
use crate::AST;
use birch::Tree;
use std::collections::HashMap;
use std::convert::TryFrom;

#[derive(Debug)]
pub enum Blueprint {
    Root,
    Num(Number),
    Str(String),
    Bool(bool),
    List,
    Dict(HashMap<String, Option<usize>>),
    Tuple(Option<String>),
    Class(String, HashMap<String, Option<usize>>),
    Take(String),
    Ident(String),
    Index(Box<Self>, Vec<usize>),
    Mod(Box<Self>, usize),
    NamedObj(String, Box<Self>),
}

impl<'s> TryFrom<&AST<'s>> for Blueprint {
    type Error = BuildError;
    fn try_from(ast: &AST<'s>) -> Result<Self, Self::Error> {
        match ast {
            AST::Str(s) => Ok(Blueprint::from(*s)),
            AST::Num(n) => Ok(Blueprint::from(*n)),
            AST::Bool(b) => Ok(Blueprint::from(*b)),
            _ => Err(BuildError::NoRef),
        }
    }
}

impl From<bool> for Blueprint {
    fn from(b: bool) -> Self {
        Blueprint::Bool(b)
    }
}

impl From<String> for Blueprint {
    fn from(s: String) -> Self {
        Blueprint::Str(s)
    }
}

impl From<&str> for Blueprint {
    fn from(s: &str) -> Self {
        Blueprint::Str(s.to_string())
    }
}

impl From<Number> for Blueprint {
    fn from(n: Number) -> Self {
        Blueprint::Num(n)
    }
}

impl Blueprint {
    pub fn from_file<P: AsRef<str>>(path: P) -> Result<(String, Tree<Self>), BuildError> {
        let mut data = String::new();
        let (path, ast) = AST::parse_file(path, &mut data)?;
        Ok((path, Self::build_tree(&ast)?))
    }

    pub fn build_tree(ast: &AST) -> Result<Tree<Self>, BuildError> {
        fn sub_build(
            tree: &mut Tree<Blueprint>,
            parent: usize,
            ast: &AST,
        ) -> Result<usize, BuildError> {
            match ast {
                // Complete types
                AST::Root(children) => {
                    assert_eq!(parent, 0, "Tried to build AST::Root at non-root vertex.");
                    for child in children {
                        sub_build(tree, parent, child)?;
                    }
                    Ok(0)
                }
                AST::Bool(_) | AST::Str(_) | AST::Num(_) => {
                    Ok(tree.add_child(parent, Blueprint::try_from(ast).expect("Blueprint build failed to convert AST Bool | Str | Num to Blueprint, which shouldn't be possible.")))
                }
                AST::List(el) => {
                    let list = tree.add_child(parent, Blueprint::List);
                    for e in el {
                        sub_build(tree, list, &e)?;
                    }
                    Ok(list)
                }
                AST::Tuple(name, elements) => {
                    let tuple = tree.add_child(parent, Blueprint::Tuple(name.map(|n| n.to_string())));
                    for e in elements {
                        sub_build(tree, tuple, &e)?;
                    }
                    Ok(tuple)
                }
                AST::Dict(m) => {
                    let dict = tree.add_child(parent, Blueprint::Dict(HashMap::new()));
                    for (k, v) in m {
                        match v {
                            Some(v) => {
                                let c = sub_build(tree, dict, v)?;
                                if let Blueprint::Dict(m) = &mut tree[dict] { m.insert((*k).to_string(), Some(c)); }
                            }
                            None => {
                                if let Blueprint::Dict(m) = &mut tree[dict] { m.insert((*k).to_string(), None); }
                            }
                        }

                    }
                    Ok(dict)
                }
                AST::Class(id, m) => {
                    let dict = tree.add_child(parent, Blueprint::Class((*id).to_string(), HashMap::new()));
                    for (k, v) in m {
                        match v {
                            Some(v) => {
                                let c = sub_build(tree, dict, v)?;
                                if let Blueprint::Class(_, m) = &mut tree[dict] { m.insert((*k).to_string(), Some(c)); }
                            }
                            None => {
                                if let Blueprint::Class(_, m) = &mut tree[dict] { m.insert((*k).to_string(), None); }
                            }
                        }

                    }
                    Ok(dict)
                }
                // Incomplete types
                AST::Ident(id) => {
                    Ok(tree.add_child(parent, Blueprint::Ident((*id).to_string())))
                }
                AST::Take(path) => {
                    // TODO :: Begin reading file asynchronously
                    Ok(tree.add_child(parent, Blueprint::Take((*path).to_string())))
                }
                AST::Index(expr, keys) => {
                    // TODO :: There must be a better way to do this
                    // Build expr -> swap dummy with built val -> swap Index with dummy
                    let index = sub_build(tree, parent, expr)?;
                    {
                        let mut dummy = Blueprint::Root;
                        let vert = tree.0.vert_mut(index);
                        vert.swap(&mut dummy);
                        // The dummy is now the expr we built
                        let mut index = Blueprint::Index(Box::new(dummy), Vec::new());
                        vert.swap(&mut index);
                        // The tree now contains the Index enum
                    }
                    for indexer in keys {
                        let indexer = sub_build(tree, index, indexer)?;
                        match &mut tree[index] {
                            Blueprint::Index(_, ref mut vec) => vec.push(indexer),
                            _ => unreachable!()
                        }
                    }
                    Ok(index)
                }
                AST::Mod(obj, m) => {
                    let obj = sub_build(tree, parent, obj)?;
                    // This is the same as Index
                    {
                        let mut dummy = Blueprint::Root;
                        let vert = tree.0.vert_mut(obj);
                        vert.swap(&mut dummy);
                        let mut obj = Blueprint::Mod(Box::new(dummy), 0);
                        vert.swap(&mut obj);
                    }
                    let m = sub_build(tree, obj, m)?;
                    match tree[obj] {
                        Blueprint::Mod(_, ref mut n) => *n = m,
                        _ => unreachable!(),
                    }
                    Ok(obj)
                }
                AST::NamedObj(name, obj) => {
                    let obj = sub_build(tree, parent, obj)?;
                    // This is the same as Index
                    {
                        let mut dummy = Blueprint::Root;
                        let vert = tree.0.vert_mut(obj);
                        vert.swap(&mut dummy);
                        let mut obj = Blueprint::NamedObj((*name).to_string(), Box::new(dummy));
                        vert.swap(&mut obj);
                    }
                    Ok(obj)
                }
                // _ => todo!("Blueprint < {} >", ast),
            }
        }
        let mut tree = Tree::new(Blueprint::Root);
        sub_build(&mut tree, 0, ast)?;
        Ok(tree)
    }
}
