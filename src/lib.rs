#![feature(or_patterns)]

#[macro_use]
extern crate pest_derive;

// mod build;
// pub use build::*;

use std::collections::HashMap;
use std::fmt::Display;

pub mod number;
use number::*;

mod deserialize;
pub use deserialize::*;

#[derive(Parser)]
#[grammar = "./stn.pest"]
struct StnParser;

#[cfg(test)]
mod tests {
    use super::*;
    use pest::Parser;
    use std::io::Read;

    fn get_read<P: AsRef<str>>(path: P) -> std::fs::File {
        std::fs::File::open(path.as_ref()).expect("Error opening file.")
    }
    fn get_data<P: AsRef<str>>(path: P) -> String {
        let mut data = String::new();
        get_read(path)
            .read_to_string(&mut data)
            .expect("Error reading file.");
        assert!(!data.is_empty());
        data
    }

    macro_rules! acc_rule {
        ($rule:path, $data:expr) => {
            let res = StnParser::parse($rule, $data);
            if let Err(e) = res {
                println!("{}", e);
                // dbg!(&e);
                panic!("Rule was rejected.");
            }
        };
    }

    macro_rules! rej_rule {
        ($rule:path, $data:expr) => {
            let res = StnParser::parse($rule, $data);
            if res.is_ok() {
                println!("Data: {}", $data);
                // dbg!(&res);
                panic!("Rule was accepted.")
            }
        };
    }

    #[test]
    fn parse_bool() {
        acc_rule!(Rule::Boolean, "true");
        acc_rule!(Rule::Boolean, "false");
        rej_rule!(Rule::Boolean, "True");
        rej_rule!(Rule::Boolean, "False");
        rej_rule!(Rule::Boolean, "");
    }

    #[test]
    fn parse_string() {
        acc_rule!(Rule::String, "\"\"");
        acc_rule!(Rule::String, "\"This is a test string\"");
        rej_rule!(Rule::String, "This is not a string");
        rej_rule!(Rule::String, "");
    }

    #[test]
    fn parse_url() {
        acc_rule!(Rule::URL, "../file.txt");
        acc_rule!(Rule::URL, "https://ashwalker.net/about.html");
    }

    #[test]
    fn parse_take() {
        acc_rule!(Rule::Take, "take \"../file.txt\"");
        // acc_rule!(Rule::Use, "use \"../file.txt\" as file");
        acc_rule!(Rule::Take, "take \"https://ashwalker.net/about.html\"");
        rej_rule!(Rule::Take, "take \"../file.txt");
    }

    #[test]
    fn parse_num() {
        acc_rule!(Rule::Int, "12");
        acc_rule!(Rule::Number, "12.6");
    }

    #[test]
    fn parse_ident() {
        acc_rule!(Rule::Ident, "two");
        acc_rule!(Rule::Ident, "two2");
    }

    #[test]
    fn parse_named_obj() {
        acc_rule!(Rule::NamedObj, "title");
        acc_rule!(
            Rule::NamedObj,
            "title: loc[\"about_title\"][env[\"locale\"]]"
        );
    }

    #[test]
    fn parse_named_obj_list() {
        acc_rule!(Rule::NamedObjList, "one, two, three: 3");
        acc_rule!(
            Rule::NamedObjList,
            "title: loc[\"about_title\"][env[\"locale\"]], style, elements: [ nav, about < { anchors: [(CW(0.5), CH(0.5))] } ]"
        );
    }

    // fn parse_unnamed_obj_list() {
    //     acc_rule!(Rule::UnnamedObjList, "CW(0.5), CH(0.5), CD(0.5)");
    // }

    #[test]
    fn parse_dict() {
        acc_rule!(Rule::Dict, "{}");
        acc_rule!(Rule::Dict, "{ }");
        acc_rule!(Rule::Dict, "{ one_obj }");
        acc_rule!(Rule::Dict, "{ ident, two: 2 }");
        acc_rule!(Rule::Dict, "{ ident, two: { obj1, obj2 }, }");
        acc_rule!(Rule::Dict, "{ title: loc[\"about_title\"][env[\"locale\"]], style, elements: [ nav, about < { anchors: [(CW(0.5), CH(0.5))] } ] }");
        acc_rule!(
            Rule::Dict,
            "{
            elements: [
                Button { link: \"about.ron\", elements: [ loc[\"about\"][env[\"locale\"]] ] },
                Button { link: \"projects.ron\", elements: [ loc[\"projects\"][env[\"locale\"]] ] }
            ]
        }"
        );
    }

    #[test]
    fn parse_list() {
        acc_rule!(Rule::List, "[]");
        acc_rule!(Rule::List, "[ ]");
        acc_rule!(Rule::List, "[ 1, 2 ]");
        acc_rule!(Rule::List, "[ 3, ident, 4.0 ]");
        acc_rule!(
            Rule::List,
            "[
            Button { link: \"about.ron\", elements: [ loc[\"about\"][env[\"locale\"]] ] },
            Button { link: \"projects.ron\", elements: [ loc[\"projects\"][env[\"locale\"]] ] }
        ]"
        );
    }

    #[test]
    fn parse_tuple() {
        acc_rule!(Rule::Tuple, "()");
        acc_rule!(Rule::Tuple, "Beef()");
        acc_rule!(Rule::Tuple, "(3, 4, 5)");
        acc_rule!(Rule::Tuple, "tuple(3, 4, 5, [6, 7], (8, 9))");
        acc_rule!(Rule::Tuple, "(CW(0.5), CH(0.5))");
    }

    #[test]
    fn parse_index() {
        rej_rule!(Rule::Index, "five[]");
        acc_rule!(Rule::Index, "three[3]");
        rej_rule!(Rule::Index, "fail[3, 4]");
        acc_rule!(Rule::Index, "acc[\"3, 4\"]");
        acc_rule!(Rule::Index, "dac[ident]");
        acc_rule!(Rule::Index, "dap[ident[3]]");
        acc_rule!(Rule::Index, "bap[\"about_title\"]");
        acc_rule!(Rule::Index, "take \"../file.txt\"[5]");
        acc_rule!(Rule::Index, "{five: 3}[\"five\"]");
        acc_rule!(Rule::Index, "[5, 4, 3][3]");
        acc_rule!(Rule::Index, "(5, 4, 3)[2]");
        acc_rule!(Rule::Index, "five[3][5]");
        acc_rule!(Rule::Index, "loc[\"projects\"][env[\"locale\"]]");
    }

    #[test]
    fn parse_class() {
        acc_rule!(Rule::Class, "beef { ident, two: { obj1, obj2 }, }");
        acc_rule!(Rule::Class, "beef2{ ident, two: { obj1, obj2 }, }");
        rej_rule!(Rule::Class, "{ ident, two: { obj1, obj2 }, }");
        acc_rule!(
            Rule::Class,
            "Button { link: \"about.ron\", elements: [ loc[\"about\"][env[\"locale\"]] ] }"
        );
    }

    #[test]
    fn parse_modifier() {
        acc_rule!(Rule::Modifier, "beef < { ident, two: { obj1, obj2 }, }");
        acc_rule!(Rule::Modifier, "beef2<{ ident, two: { obj1, obj2 }, }");
        rej_rule!(Rule::Modifier, "{ ident, two: { obj1, obj2 }, }");
        rej_rule!(Rule::Modifier, "beef3 < ");
    }

    #[test]
    fn parse_stn() {
        acc_rule!(Rule::Stn, "[1, 2, 3]; [4, 5, 6];");
        rej_rule!(Rule::Stn, "[1, 2, 3]; [4, 5, 6]");
        acc_rule!(Rule::Stn, "nav: take \"nav.stn\";");
        acc_rule!(Rule::Stn, "take \"nav.stn\";");
        acc_rule!(Rule::Stn, "nav: take \"nav.stn\"; { test };");
        rej_rule!(Rule::Stn, "nav: take \"nav.stn\"; { test }");
        let about = get_data("./data/ashwalker.net/about.stn");
        acc_rule!(Rule::Stn, &about);
        let nav = get_data("./data/ashwalker.net/nav.stn");
        acc_rule!(Rule::Stn, &nav);
    }

    // #[test]
    // fn parse_ast() {
    //     let data = get_data("./data/ashwalker.net/about.stn");
    //     // dbg!(&data);
    //     let ast1 = AST::parse(&data).unwrap();
    //     // dbg!(&ast1);
    //     // let s_ast1 = ast1.to_string();
    //     // eprintln!("AST 1:\n{}", s_ast1);
    //     // let ast2 = AST::parse(&s_ast1).unwrap();
    //     // eprintln!("AST 2:\n{}", ast2);
    // }
}
