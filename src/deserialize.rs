use crate::*;
use birch::Tree;
use std::convert::TryFrom;
use std::iter::FromIterator;

mod file_reader;
pub use file_reader::*;

mod blueprint;
pub use blueprint::*;

mod build_error;
pub use build_error::*;

mod ast;
pub use ast::*;

#[derive(Debug, Clone)]
pub enum Stone {
    Dummy,
    Root(Vec<Self>, HashMap<String, Self>),
    Num(Number),
    Str(String),
    Bool(bool),
    List(Vec<Self>),
    Dict(HashMap<String, Self>),
    Tuple(Option<String>, Vec<Self>),
    Class(String, HashMap<String, Self>),
    // Unfinished
    Named(String, Box<Self>),
    Ident(String),
    Mod(Box<Self>, Box<Self>),
    Index(Box<Self>, Vec<Self>),
}

impl Stone {
    pub fn take(&mut self) -> Self {
        let mut dummy = Stone::Dummy;
        std::mem::swap(self, &mut dummy);
        dummy
    }

    pub fn try_index(&self, i: usize) -> Result<&Self, ()> {
        match self {
            Stone::Root(v, _) | Stone::List(v) | Stone::Tuple(_, v) => Ok(&v[i]),
            _ => Err(()),
        }
    }

    pub fn try_index_mut(&mut self, i: usize) -> Result<&mut Self, ()> {
        match self {
            Stone::Root(v, _) | Stone::List(v) | Stone::Tuple(_, v) => Ok(&mut v[i]),
            _ => Err(()),
        }
    }

    pub fn try_get(&self, k: &str) -> Result<&Self, ()> {
        match self {
            Stone::Root(_, m) | Stone::Dict(m) | Stone::Class(_, m) => Ok(&m[k]),
            _ => Err(()),
        }
    }

    pub fn try_get_mut(&mut self, k: &str) -> Result<&mut Self, ()> {
        match self {
            Stone::Root(_, m) | Stone::Dict(m) | Stone::Class(_, m) => Ok(m.get_mut(k).unwrap()),
            _ => Err(()),
        }
    }

    pub fn build<F: FileReader>(
        ast: &AST,
        file_reader: &mut Option<&mut F>,
    ) -> Result<Self, BuildError> {
        fn collapse<F: FileReader>(
            reader: &mut Option<&mut F>,
            tree: &Tree<Blueprint>,
            current: usize,
            replace: Option<&Blueprint>,
            ignore: Vec<usize>,
        ) -> Result<Stone, BuildError> {
            use Blueprint::*;
            match replace.unwrap_or(&tree[current]) {
                Bool(b) => Ok(Stone::Bool(*b)),
                Str(s) => Ok(Stone::Str(s.to_string())),
                Num(n) => Ok(Stone::Num(*n)),
                Root => {
                    let mut v = Vec::new();
                    let mut m = HashMap::new();
                    for child in tree.children(current) {
                        if ignore.contains(&child) {
                            continue;
                        }
                        let child = collapse(reader, tree, child, None, Vec::new())?;
                        match child {
                            Stone::Named(id, s) => {
                                m.insert(id, *s);
                            }
                            _ => v.push(child),
                        }
                    }
                    Ok(Stone::Root(v, m))
                }
                List => {
                    let mut list = Vec::new();
                    for child in tree.children(current) {
                        if ignore.contains(&child) {
                            continue;
                        }
                        list.push(collapse(reader, tree, child, None, Vec::new())?);
                    }
                    Ok(Stone::List(list))
                }
                Tuple(id) => {
                    let mut list = Vec::new();
                    for child in tree.children(current) {
                        if ignore.contains(&child) {
                            continue;
                        }
                        list.push(collapse(reader, tree, child, None, Vec::new())?);
                    }
                    Ok(Stone::Tuple(id.clone(), list))
                }
                Dict(m) => {
                    let mut dict = HashMap::new();
                    for (k, v) in m {
                        dict.insert(
                            k.clone(),
                            collapse(reader, tree, v.unwrap(), None, Vec::new())?,
                        );
                    }
                    Ok(Stone::Dict(dict))
                }
                Class(id, m) => {
                    let mut dict = HashMap::new();
                    for (k, v) in m {
                        dict.insert(
                            k.clone(),
                            collapse(reader, tree, v.unwrap(), None, Vec::new())?,
                        );
                    }
                    Ok(Stone::Class(id.clone(), dict))
                }
                Take(p) => match reader.as_mut() {
                    Some(r) => Ok(Stone::build(&AST::parse(p, &r.read_file(p)?)?, reader)?),
                    None => Err(BuildError::NoImport),
                },
                Mod(obj, m) => {
                    let left = collapse(reader, tree, current, Some(obj), vec![*m])?;
                    let right = collapse(reader, tree, *m, None, Vec::new())?;
                    Ok(Stone::Mod(Box::new(left), Box::new(right)))
                }
                Index(obj, i) => {
                    let left = collapse(reader, tree, current, Some(obj), i.clone())?;
                    let mut indexers = Vec::new();
                    for indexer in i {
                        indexers.push(collapse(reader, tree, *indexer, None, Vec::new())?);
                    }
                    Ok(Stone::Index(Box::new(left), indexers))
                }
                NamedObj(name, obj) => Ok(Stone::Named(
                    name.to_string(),
                    Box::new(collapse(reader, tree, current, Some(obj), Vec::new())?),
                )),
                Ident(id) => Ok(Stone::Ident(id.to_string())),
                // _ => todo!("Collapse < {:?} >", tree[current]),
            }
        }
        fn finish(idents: &HashMap<String, Stone>, stone: &mut Stone) -> Result<(), BuildError> {
            // println!("Finishing {:?}", stone);
            loop {
                match stone {
                    Stone::Ident(id) => {
                        // println!("Finishing ident {}...", id);
                        let mut obj = match idents.get(id) {
                            Some(o) => o.clone(),
                            None => panic!("Ident not found: {}", id),
                        };
                        std::mem::swap(&mut obj, stone);
                    }
                    Stone::Mod(left, right) => {
                        // println!("Finishing mod...");
                        let mut right = right.take();
                        let mut left = left.take();
                        finish(idents, &mut left)?;
                        finish(idents, &mut right)?;
                        left.merge(right)?;
                        std::mem::swap(stone, &mut left);
                    }
                    Stone::Index(left, indexers) => {
                        // println!("Finishing index...");
                        let mut left = left.take();
                        // Collapse indexer from left to right (like a telescope)
                        for indexer in indexers {
                            finish(idents, &mut left)?;
                            finish(idents, indexer)?;
                            match indexer {
                                Stone::Str(s) => match left.try_get(s) {
                                    Ok(s) => left = s.clone(),
                                    Err(_) => {
                                        eprintln!("Couldn't index {} into {:?}", s, left);
                                        return Err(BuildError::Indexer);
                                    }
                                },
                                Stone::Num(n) => match left.try_index(n.up_to_usize().unwrap()) {
                                    Ok(s) => left = s.clone(),
                                    Err(_) => {
                                        eprintln!("Couldn't index {} into {:?}", n, left);
                                        return Err(BuildError::Indexer);
                                    }
                                },
                                _ => return Err(BuildError::Indexer),
                            }
                        }
                        std::mem::swap(&mut left, stone);
                    }
                    Stone::List(v) | Stone::Tuple(_, v) => {
                        for stone in v {
                            finish(idents, stone)?;
                        }
                    }
                    Stone::Dict(m) | Stone::Class(_, m) => {
                        for (_, stone) in m {
                            finish(idents, stone)?;
                        }
                    }
                    Stone::Root(v, m) => {
                        for stone in v.iter_mut().chain(m.values_mut()) {
                            finish(idents, stone)?;
                        }
                    }
                    Stone::Named(_, stone) => finish(idents, stone.as_mut())?,
                    _ => (),
                }
                // println!("Check finished...");
                if stone.finished() {
                    break;
                }
            }
            Ok(())
        }
        let tree = Blueprint::build_tree(ast)?;
        // dbg!(&tree);
        let mut res = collapse(file_reader, &tree, 0, None, Vec::new())?;
        let mut idents = match &res {
            Stone::Root(_, idents) => {
                HashMap::from_iter(idents.iter().map(|(k, v)| (k.to_string(), v.clone())))
            }
            _ => unreachable!(),
        };
        if !idents.contains_key("env") {
            idents.insert(
                String::from("env"),
                Stone::Dict(HashMap::from_iter(vec![(
                    "locale".to_string(),
                    Stone::Str(String::from("en_us")),
                )])),
            );
        }
        finish(&idents, &mut res)?;
        Ok(res)
    }

    fn finished(&self) -> bool {
        match self {
            Stone::Ident(_) | Stone::Mod(_, _) | Stone::Index(_, _) => false,
            _ => true,
        }
    }

    pub fn merge(&mut self, other: Stone) -> Result<(), BuildError> {
        match self {
            Stone::Root(v, m) => match other {
                Stone::Root(mut ov, mut om) => {
                    v.append(&mut ov);
                    for (k, v) in om.drain() {
                        m.insert(k, v);
                    }
                }
                Stone::List(mut o) => v.append(&mut o),
                Stone::Class(_, mut o) | Stone::Dict(mut o) => {
                    for (k, v) in o.drain() {
                        m.insert(k, v);
                    }
                }
                _ => return Err(BuildError::HeteroMod),
            },
            Stone::List(v) => match other {
                Stone::List(mut o) => v.append(&mut o),
                _ => return Err(BuildError::HeteroMod),
            },
            Stone::Class(_, m) | Stone::Dict(m) => match other {
                Stone::Class(_, mut o) | Stone::Dict(mut o) => {
                    for (k, v) in o.drain() {
                        m.insert(k, v);
                    }
                }
                _ => return Err(BuildError::HeteroMod),
            },
            _ => return Err(BuildError::Unmoddable),
        }
        Ok(())
    }

    pub fn as_vec_mut(&mut self) -> Result<&mut Vec<Self>, ()> {
        match self {
            Stone::Root(v, _) | Stone::List(v) => Ok(v),
            _ => Err(()),
        }
    }

    pub fn as_dict_mut(&mut self) -> Result<&mut HashMap<String, Self>, ()> {
        match self {
            Stone::Root(_, m) | Stone::Dict(m) => Ok(m),
            _ => Err(()),
        }
    }
}

impl Display for Stone {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        fn disp(stone: &Stone, f: &mut std::fmt::Formatter, level: usize) -> std::fmt::Result {
            use Stone::*;
            let tab = level * 4;
            match stone {
                Str(s) => write!(f, "{}", s)?,
                Num(n) => write!(f, "{}", n)?,
                Bool(b) => write!(f, "{}", b)?,
                Root(v, m) => {
                    for (name, stone) in m {
                        write!(f, "{}: ", name)?;
                        disp(stone, f, level + 1)?;
                        writeln!(f, ";")?;
                    }
                    for stone in v {
                        disp(stone, f, level + 1)?;
                        writeln!(f, ";")?;
                    }
                }
                List(v) => {
                    writeln!(f, "{:tab$}[", "", tab = tab)?;
                    for stone in v {
                        write!(f, "{:tab$}", "", tab = tab)?;
                        disp(stone, f, level + 1)?;
                        writeln!(f, ",")?;
                    }
                    write!(f, "{:tab$}]", "", tab = tab)?;
                }
                Tuple(id, v) => {
                    writeln!(
                        f,
                        "{:tab$}{}(",
                        "",
                        id.as_ref().map_or("", |s| s.as_str()),
                        tab = tab
                    )?;
                    for stone in v {
                        write!(f, "{:tab$}", "", tab = tab)?;
                        disp(stone, f, level + 1)?;
                        writeln!(f, ",")?;
                    }
                    write!(f, "{:tab$})", "", tab = tab)?;
                }
                Dict(m) => {
                    writeln!(f, "{:tab$}{{", "", tab = tab)?;
                    for (k, stone) in m {
                        write!(f, "{:tab$}{}: ", "", k, tab = tab)?;
                        disp(stone, f, level + 1)?;
                        writeln!(f, ",")?;
                    }
                    write!(f, "{:tab$}}}", "", tab = tab)?;
                }
                _ => write!(f, "{:?}", stone)?,
            }
            // if children.is_empty() {
            //     writeln!(f, "{:tab$}{},", "", tree[current], tab = tab)?;
            // } else {
            //     writeln!(f, "{:tab$}{} <", "", tree[current], tab = tab)?;
            //     for child in tree.children(current) {
            //         disp(tree, f, child, level + 1)?;
            //     }
            //     writeln!(f, "{:tab$}>,", "", tab = tab)?;
            // }
            Ok(())
        }
        disp(self, f, 0)
    }
}

macro_rules! stone_into {
    ($s:pat, $t:ty, $c:expr) => {
        #[allow(unused_parens)]
        impl TryFrom<Stone> for $t {
            type Error = Stone;
            fn try_from(s: Stone) -> Result<Self, Self::Error> {
                match s {
                    $s => $c,
                    _ => Err(s),
                }
            }
        }
    };
}

macro_rules! stone_into_ref {
    ($s:pat, $t:ty, $c:expr) => {
        #[allow(unused_parens)]
        impl<'s> TryFrom<&'s Stone> for &'s $t {
            type Error = ();
            fn try_from(s: &'s Stone) -> Result<Self, Self::Error> {
                match s {
                    $s => $c,
                    _ => Err(()),
                }
            }
        }
    };
}

impl TryFrom<&Stone> for bool {
    type Error = ();
    fn try_from(s: &Stone) -> Result<Self, Self::Error> {
        match s {
            Stone::Bool(b) => Ok(*b),
            _ => Err(()),
        }
    }
}
impl TryFrom<&Stone> for Number {
    type Error = ();
    fn try_from(s: &Stone) -> Result<Self, Self::Error> {
        match s {
            Stone::Num(n) => Ok(*n),
            _ => Err(()),
        }
    }
}
stone_into!(Stone::Str(s), String, Ok(s));
stone_into!(
    (Stone::Root(v, _) | Stone::List(v) | Stone::Tuple(_, v)),
    Vec<Stone>,
    Ok(v)
);
stone_into!((Stone::Root(_, m) | Stone::Dict(m) | Stone::Class(_, m)), HashMap<String, Stone>, Ok(m));

stone_into_ref!(Stone::Str(ref s), str, Ok(s));
stone_into_ref!(
    (Stone::Root(ref v, _) | Stone::List(ref v) | Stone::Tuple(_, ref v)),
    Vec<Stone>,
    Ok(v)
);
stone_into_ref!((Stone::Root(_, ref m) | Stone::Dict(ref m) | Stone::Class(_, ref m)), HashMap<String, Stone>, Ok(m));

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Read;

    fn get_read() -> std::fs::File {
        std::fs::File::open("./data/ashwalker.net/about.stn").expect("Error opening file.")
    }
    fn get_data() -> String {
        let mut data = String::new();
        get_read()
            .read_to_string(&mut data)
            .expect("Error reading file.");
        assert!(!data.is_empty());
        data
    }

    #[test]
    fn build_stone() {
        let data = get_data();
        let mut reader = FSReader;
        dbg!(Stone::build(
            &AST::parse("data/ashwalker.net/about.stn", &data).unwrap(),
            &mut Some(&mut reader)
        )
        .unwrap());
    }
}
